sudo yum install -y rpm-build

sudo rpm --import http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
sudo yum install -y http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

mkdir -p ${HOME}/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS}
echo "%_topdir ${HOME}/rpmbuild" > ${HOME}/.rpmmacros
echo '%dist .el6.abril' >> ${HOME}/.rpmmacros

sudo yum install -y libyaml gcc GeoIP-devel gd-devel libxslt-devel openssl-devel pcre-devel perl-devel 'perl(ExtUtils::Embed)' zlib-devel gcc-c++ make libcurl-devel

cp /vagrant/*.spec rpmbuild/SPECS/
cp /vagrant/sources/* rpmbuild/SOURCES/

sudo yum remove -y ruby-1.8.7.352-7.el6_2 rubygems-1.3.7-1.el6 ruby-irb-1.8.7.352-7.el6_2 ruby-libs-1.8.7.352-7.el6_2 ruby-rdoc-1.8.7.352-7.el6_2

sudo /bin/rpm --force --nodeps -ivh http://172.16.2.238/ubber-site/prod/ruby-2.0.0_p247-1.el6.x86_64.rpm

rpmbuild -ba ${HOME}/rpmbuild/SPECS/nginx.spec

/bin/cp -f -v ${HOME}/rpmbuild/RPMS/x86_64/* /vagrant/rpms/
/bin/cp -f -v ${HOME}/rpmbuild/SRPMS/* /vagrant/rpms/

# vim:ft=shell
