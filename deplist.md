Nginx from EPEL
===============
 GeoIP                                        x86_64                                1.4.8-1.el6                                         epel                                   620 k
 fontconfig                                   x86_64                                2.8.0-3.el6                                         base                                   186 k
 freetype                                     x86_64                                2.3.11-14.el6_3.1                                   updates                                359 k
 gd                                           x86_64                                2.0.35-11.el6                                       base                                   142 k
 libX11                                       x86_64                                1.5.0-4.el6                                         base                                   584 k
 libX11-common                                noarch                                1.5.0-4.el6                                         base                                   192 k
 libXau                                       x86_64                                1.0.6-4.el6                                         base                                    24 k
 libXpm                                       x86_64                                3.5.10-2.el6                                        base                                    51 k
 libjpeg-turbo                                x86_64                                1.2.1-1.el6                                         base                                   174 k
 libpng                                       x86_64                                2:1.2.49-1.el6_2                                    base                                   182 k
 libxcb                                       x86_64                                1.8.1-1.el6                                         base                                   110 k
 libxslt

My Nginx
========
 GeoIP                                   x86_64                       1.4.8-1.el6                              epel                                                            620 k
 fontconfig                              x86_64                       2.8.0-3.el6                              base                                                            186 k
 freetype                                x86_64                       2.3.11-14.el6_3.1                        updates                                                         359 k
 gd                                      x86_64                       2.0.35-11.el6                            base                                                            142 k
 libX11                                  x86_64                       1.5.0-4.el6                              base                                                            584 k
 libX11-common                           noarch                       1.5.0-4.el6                              base                                                            192 k
 libXau                                  x86_64                       1.0.6-4.el6                              base                                                             24 k
 libXpm                                  x86_64                       3.5.10-2.el6                             base                                                             51 k
 libjpeg-turbo                           x86_64                       1.2.1-1.el6                              base                                                            174 k
 libpng                                  x86_64                       2:1.2.49-1.el6_2                         base                                                            182 k
 libxcb                                  x86_64                       1.8.1-1.el6                              base                                                            110 k
 libxslt                                 x86_64                       1.1.26-2.el6_3.1                         base                                                            452 k
 libyaml                                 x86_64                       0.1.3-1.el6                              epel                                                             52 k
 make                                    x86_64                       1:3.81-20.el6                            base                                                            389 k
 pkgconfig                               x86_64                       1:0.23-9.1.el6                           base                                                             70 k
 ruby                                    x86_64                       2.0.0_p247-1.el6                         ubbersite                                                        20 M
 rubygem-passenger                       x86_64                       4.0.14-1.el6.abril                       ubbersite                                                        13 M
Updating for dependencies:
 zlib                                    x86_64                       1.2.3-29.el6                             base                                                             73 k
